package keystore;

import java.util.*;
import java.lang.Object;

public class KeyStore02 extends java.lang.Object {
	private LinkedList<String> array;

	KeyStore02() {
		array = new LinkedList<String>();
	}

	public void clear() {

		array.clear();

	}

	public String getIndex(int index) {

		return array.get(index);

	}

	public int indexOf(String e) {
		return array.indexOf(e);
	}

	public void remove(int i) {
		array.remove(i);

	}

	public void remove(String e) {
		array.remove(array.indexOf(e));
	}

	public int size() {
		return array.size();
	}

	public boolean add(String e) {
		return array.add(e);
	}

	public String toString() {
		return array.toString();
	}

}
