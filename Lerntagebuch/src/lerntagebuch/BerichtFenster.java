package lerntagebuch;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import javax.swing.JTextField;

public class BerichtFenster extends JFrame {

	private JPanel contentPane;
    private Logic lg;
    private JTextField Dauer;
    private JTextField Tag;
    
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public BerichtFenster(Logic lg) {
		this.lg = lg;
		int dauer = 0;
		String s = "";
		double durchschnitt = 0;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel North = new JPanel();
		contentPane.add(North, BorderLayout.NORTH);
		
		JLabel lblBericht = new JLabel("Bericht");
		lblBericht.setFont(new Font("Consolas", Font.PLAIN, 18));
		North.add(lblBericht);
		
		JPanel South = new JPanel();
		contentPane.add(South, BorderLayout.SOUTH);
		
		JButton btnBeenden = new JButton("Beenden");
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		South.add(btnBeenden);
		
		JPanel Center = new JPanel();
		contentPane.add(Center, BorderLayout.CENTER);
		Center.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel = new JPanel();
		Center.add(panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Dauer gesammt");
		panel.add(lblNewLabel);
		
		
		
		Dauer = new JTextField();
		Dauer.setEditable(false);
		panel.add(Dauer);
		Dauer.setColumns(10);
		// Dauer ausrechnen
				for(Lerneintrag le : lg.temp){
					dauer = dauer + le.getDauer();
				}
				s = "" + dauer;
				Dauer.setText(s);
		
		JLabel lblNewLabel_1 = new JLabel("Tagesdurchschnitt");
		panel.add(lblNewLabel_1);
		
		Tag = new JTextField();
		Tag.setEditable(false);
		panel.add(Tag);
		int i = 0;
		for(Lerneintrag le : lg.temp){
			i ++;
			durchschnitt = durchschnitt + le.getDauer();
		
		}
		durchschnitt = durchschnitt / i;
		s = "" + durchschnitt;
	Tag.setText(s);
		Tag.setColumns(10);
	}

}

