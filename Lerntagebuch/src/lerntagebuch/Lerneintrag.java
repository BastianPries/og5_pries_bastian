package lerntagebuch;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Lerneintrag {
	private Date datum;
	private String fach;
	private String beschreibung;
	private int dauer;

	public Lerneintrag(Date datum, String fach, String beschreibung, int dauer) {
		this.setDatum(datum);
		this.setFach(fach);
		this.setBeschreibung(beschreibung);
		this.setDauer(dauer);

	}

	public Lerneintrag() {
	}

	public String getFach() {
		return fach;
	}

	public void setFach(String fach) {
		this.fach = fach;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public String toString() {
		DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
String d = df.format(datum);
String b = ("\r" + d + "\r" + this.fach +"\r" + this.beschreibung +"\r" + this.dauer);
		return b;
	}

}
