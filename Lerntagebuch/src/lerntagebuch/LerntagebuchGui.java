package lerntagebuch;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JMenu;

public class LerntagebuchGui extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private Logic lg;
	private BerichtFenster br;
	//private List<Lerneintrag> list = new ArrayList<Lerneintrag>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LerntagebuchGui frame = new LerntagebuchGui(new Logic(new FileControl()));
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public LerntagebuchGui(Logic lg) {
		this.lg = lg;
		try {
			lg.reloadFromDisk();
		} catch (Exception e) {
			e.printStackTrace();
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 459, 384);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel pnl_Label = new JPanel();
		pnl_Label.setBackground(Color.LIGHT_GRAY);
		contentPane.add(pnl_Label, BorderLayout.NORTH);
		pnl_Label.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel = new JLabel("Lerntagebuch von " + lg.getLearnerName());
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		pnl_Label.add(lblNewLabel, BorderLayout.WEST);

		JPanel pnl_south = new JPanel();
		contentPane.add(pnl_south, BorderLayout.SOUTH);
		pnl_south.setLayout(new BorderLayout(0, 0));

		JPanel pnl_butons = new JPanel();
		pnl_south.add(pnl_butons, BorderLayout.EAST);
		pnl_butons.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnNewButton = new JButton("Neuer Eintrag");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lg.NeuerEintrag();
			}
		});
		pnl_butons.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Bericht");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == btnNewButton_1) {
					lg.BerichtErstellen();
				}
			}
		});
		pnl_butons.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("beenden");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);
			}
		});
		pnl_butons.add(btnNewButton_2);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				LerntagebuchGui b = new LerntagebuchGui(new Logic(new FileControl()));
				try {
					lg.reloadFromDisk();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				b.setVisible(true);
				
			}
		});
		pnl_south.add(btnRefresh, BorderLayout.CENTER);

		JPanel pnl_center = new JPanel();
		contentPane.add(pnl_center, BorderLayout.CENTER);
		pnl_center.setLayout(new BorderLayout(0, 0));

		JPanel pnl_Tabellenheader = new JPanel();
		pnl_center.add(pnl_Tabellenheader, BorderLayout.NORTH);
		pnl_Tabellenheader.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnl_center.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setModel(
				new DefaultTableModel(new Object[][] {}, new String[] { "Datum", "Fach", "Dauer", "Aktivitšt" }));

		scrollPane.setViewportView(table);
		addEntries();
	}

	public void addEntries() {
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		//list = lg.getListEntries();
		
		for(Lerneintrag le : lg.temp){
			DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
			String d = df.format(le.getDatum());
			model.addRow(new Object[]{d, le.getFach(), le.getDauer(), le.getBeschreibung()});
			
			
		}
		table.setModel(model);
	}
}
