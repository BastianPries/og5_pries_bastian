package lerntagebuch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import javax.swing.table.DefaultTableModel;

public class Logic {
	FileControl filecontr;
	List<Lerneintrag> temp = new ArrayList<Lerneintrag>();

	public Logic(FileControl fc) {
		this.filecontr = fc;
	}

	public void reloadFromDisk() throws IOException {
		try {
			File f = new File("F:/Schule/miriam.dat");
			filecontr.Ea(f);
			this.temp = filecontr.eintraege;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getLearnerName() {
		String s = "";
		try {
			FileReader fr = new FileReader("F:/Schule/miriam.dat");
			BufferedReader br = new BufferedReader(fr);

			s = br.readLine();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;

	}
	public List<Lerneintrag> getListEntries(){
		try {
			this.reloadFromDisk();
		}
		
		
		catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
		
	}
	public void BerichtErstellen(){
		BerichtFenster frame = new BerichtFenster(this);
		frame.setVisible(true);
		
	
		
	}
	
	public void NeuerEintrag(){
		NeuerEintragGui frame = new NeuerEintragGui(this);
		frame.setVisible(true);
		
	}
	
	public List getList(){
		return temp;
	};
	

	
}