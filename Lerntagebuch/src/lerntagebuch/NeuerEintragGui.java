package lerntagebuch;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

public class NeuerEintragGui extends JFrame {

	private JPanel contentPane;
	private JTextField txt_Datum;
	private JTextField txt_Fach;
	private JTextField txt_Dauer;
	private JTextField txt_Aktivitšt;
	 Logic lg;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public NeuerEintragGui(Logic lg) {
		this.lg = lg;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel North = new JPanel();
		North.setBackground(Color.LIGHT_GRAY);
		contentPane.add(North, BorderLayout.NORTH);

		JLabel lblNeuerEintrag = new JLabel("Neuer Eintrag");
		lblNeuerEintrag.setFont(new Font("Consolas", Font.PLAIN, 18));
		North.add(lblNeuerEintrag);

		JPanel Center = new JPanel();
		contentPane.add(Center, BorderLayout.CENTER);
		Center.setLayout(new GridLayout(4, 2, 0, 0));

		JLabel lbl_Datum = new JLabel("Datum:");
		Center.add(lbl_Datum);

		txt_Datum = new JTextField();
		Center.add(txt_Datum);
		txt_Datum.setColumns(10);

		JLabel lbl_Fach = new JLabel("Fach:");
		Center.add(lbl_Fach);

		txt_Fach = new JTextField();
		Center.add(txt_Fach);
		txt_Fach.setColumns(10);

		JLabel lbl_Dauer = new JLabel("Dauer:");
		Center.add(lbl_Dauer);

		txt_Dauer = new JTextField();
		Center.add(txt_Dauer);
		txt_Dauer.setColumns(10);

		JLabel lbl_Aktivitaet = new JLabel("Aktivit\u00E4t:");
		Center.add(lbl_Aktivitaet);

		txt_Aktivitšt = new JTextField();
		Center.add(txt_Aktivitšt);
		txt_Aktivitšt.setColumns(10);

		JPanel South = new JPanel();
		contentPane.add(South, BorderLayout.SOUTH);

		JButton btn_Erstellen = new JButton("Eintrag Erstellen");
		btn_Erstellen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
				
					try {
						Date date = df.parse(txt_Datum.getText());
						int Dauer = Integer.parseInt(txt_Dauer.getText());
						String fach = txt_Fach.getText();
						String Beschreibung = txt_Aktivitšt.getText();
						lg.filecontr.NeuerEintrag(date, fach, Beschreibung, Dauer);
				
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			
			
				


				;

			}
		});
		South.add(btn_Erstellen);

		JButton btn_Beenden = new JButton("Beenden");
		btn_Beenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		South.add(btn_Beenden);
	}

}
