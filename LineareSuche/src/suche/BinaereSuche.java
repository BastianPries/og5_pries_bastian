package suche;

public class BinaereSuche {
	static long Suche(long[] array, long n) {
		StopUhr Uhr = new StopUhr();
		long erg = 0;

		Uhr.starten();
		int l = (array.length / 2);
		int al = array.length - 1;
		int zwerg = 0;

		if (n > array[array.length - 1] | n < array[0]) {
			System.out.println("zahl nicht gefunden");
			System.exit(1);
		}

		for (int i = 2; i < array.length; i++) {

			if (array[l - 1] == n) {
				erg = l - 1;
			}

			else if (array[l - 1] < n) {
				zwerg = (int) (al / Math.pow(2, i));
				if (zwerg == 0) {
					zwerg = 1;
				}

				l = l + zwerg;
			}

			else {
				zwerg = (int) (al / Math.pow(2, i));
				if (zwerg == 0) {
					zwerg = 1;
				}

				l = l - zwerg;

			}

		}
		Uhr.beenden();
		System.out.println("es dauerte " + Uhr.getDauer() + " ms ");
		
		return erg;

	}
}
