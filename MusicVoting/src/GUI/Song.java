package GUI;

public class Song {
private String Name;
private String Artist;
private String Genre;
private int Votes;
public int getVotes() {
	return Votes;
}
public void setVotes(int votes) {
	Votes = votes;
}
public String getGenre() {
	return Genre;
}
public void setGenre(String genre) {
	Genre = genre;
}
public String getArtist() {
	return Artist;
}
public void setArtist(String artist) {
	Artist = artist;
}
public String getName() {
	return Name;
}
public void setName(String name) {
	Name = name;
}
}
