package GUI;

import java.util.LinkedList;
import java.util.List;



public class SongList {
	/**
	 * Bezeichnung der Attribute der Klasse Kunde stellt für einen JTable die
	 * Spaltennamen bereit
	 */
	public static final String[] SPALTENNAMEN = { "Name", "Artist", "Genre", "Votes"};
	private List<Song> songlist;

	public SongList() {
		this.songlist = new LinkedList<Song>();
	}

	public void addSong(Song s) {
		this.songlist.add(s);
	}

	public List<Song> getSongList() {
		return songlist;
	}

	public void setSongList(List<Song> songlist) {
		this.songlist = songlist;
	}

	/**
	 * stellt für ein JTable die Tabellendaten bereit
	 * 
	 * @return
	 */
	public Object[][] getSongs() {
		Object[][] tabledata = new Object[songlist.size()][SPALTENNAMEN.length];
		for (int i = 0; i < songlist.size(); i++) {
			tabledata[i][0] = songlist.get(i).getName() + "";
			tabledata[i][1] = songlist.get(i).getArtist();
			tabledata[i][2] = songlist.get(i).getGenre();
			tabledata[i][3] = songlist.get(i).getVotes();
		}
		return tabledata;
	}
}