package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.GridLayout;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class VotingGui extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VotingGui frame = new VotingGui();
					frame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the frame.
	 */
	public VotingGui() {
		setBackground(Color.PINK);
		setTitle("Music Voting");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel pnl_North = new JPanel();
		pnl_North.setBackground(Color.PINK);
		contentPane.add(pnl_North, BorderLayout.NORTH);
		
		JLabel lblPlaylist = new JLabel("Playlist");
		pnl_North.add(lblPlaylist);
		
		JPanel pnl_west = new JPanel();
		pnl_west.setBackground(Color.PINK);
		contentPane.add(pnl_west, BorderLayout.WEST);
		
		JButton btn_Musikwunsch = new JButton("Musikwunsch");
		btn_Musikwunsch.setBackground(Color.PINK);
		btn_Musikwunsch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Wunsch frame = new Wunsch();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		
		JButton btn_MusikVote = new JButton("MusikVote");
		btn_MusikVote.setBackground(Color.PINK);
		
		JButton btn_PlaylistAusg = new JButton("Playlist");
		btn_PlaylistAusg.setBackground(Color.PINK);
		btn_PlaylistAusg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//logic.getPlaylist
			}
		});
		pnl_west.setLayout(new GridLayout(0, 1, 0, 0));
		pnl_west.add(btn_Musikwunsch);
		pnl_west.add(btn_MusikVote);
		pnl_west.add(btn_PlaylistAusg);
		Song STFU = new Song();
		STFU.setArtist("Pink Guy");
		STFU.setName("SFTU");
		STFU.setGenre("Filth");
	    STFU.setVotes(9001);
	    SongList Songs = new SongList();
	    Songs.addSong(STFU);
		
	
		table = new JTable(Songs.getSongs(), SongList.SPALTENNAMEN );
		table.setForeground(Color.BLACK);
		table.setBackground(Color.PINK);
		table.setFillsViewportHeight(true);
		table.setToolTipText("");
		table.getTableHeader().setBackground(Color.PINK);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setToolTipText("");
		contentPane.add(scrollPane, BorderLayout.CENTER);
	}

}
