package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;

public class Wunsch extends JFrame {

	private JPanel contentPane;
	private JTextField txt_Name;
	private JTextField txt_Artist;
	private JTextField txt_Genre;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Wunsch frame = new Wunsch();
					frame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Wunsch() {
		setResizable(false);
		setTitle("MusikWunsch");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 321, 160);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(0, 2, 2, 0));
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_1.add(lblNewLabel);
		
		txt_Name = new JTextField();
		panel_1.add(txt_Name);
		txt_Name.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Artist");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_1.add(lblNewLabel_1);
		
		txt_Artist = new JTextField();
		panel_1.add(txt_Artist);
		txt_Artist.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Genre");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_1.add(lblNewLabel_2);
		
		txt_Genre = new JTextField();
		panel_1.add(txt_Genre);
		txt_Genre.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3, BorderLayout.EAST);
		
		JButton btn_bestaetigen = new JButton("Best\u00E4tigen");
		panel_3.add(btn_bestaetigen);
		btn_bestaetigen.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn_bestaetigen.setVerticalAlignment(SwingConstants.TOP);
		
		JButton btn_Zur�ck = new JButton("Zur\u00FCck");
		panel_3.add(btn_Zur�ck);
		btn_Zur�ck.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn_Zur�ck.setVerticalAlignment(SwingConstants.TOP);
		btn_Zur�ck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btn_bestaetigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
		    SQL.addSong(txt_Name.getText(),txt_Artist.getText(),txt_Genre.getText());
			
			}
		});
	}

}
