package sort;

	public class QuickSort {
		
		private int Vert = 0;
		
		public void quickSort(int[] arr, int low, int high) {
			
			if (arr == null || arr.length == 0)
				return;
	 
			if (low >= high)
				return;
	 
			
			int middle = low + (high - low) / 2;
			int vert = 0;
			int pivot = arr[middle];
			System.out.println("der Pivotpoint ist: " + pivot );
	
			int i = low, j = high;
			while (i <= j) {
				while (arr[i] < pivot) {
					i++;
				}
	 
				while (arr[j] > pivot) {
					j--;
				}
	 
				if (i <= j) {
					System.out.println(arr[i] + " mit " + arr[j] + " getauscht");
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
					i++;
					j--;
				this.Vert = this.Vert + 1;
					
				}
			}
	 
		
		
				quickSort(arr, low, j);
	 

				quickSort(arr, i, high);
		
	}
		public int getVert(){
			return this.Vert;
		}
	}