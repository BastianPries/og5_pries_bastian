package sort;

public class StopUhr {
	
		private long startpunkt;
		private long endpunkt;

		public StopUhr() {
		}

		public void starten() {
			this.startpunkt = System.currentTimeMillis();
		}

		public void beenden() {
			this.endpunkt = System.currentTimeMillis();
		}

		public long getDauer() {
			return this.endpunkt - this.startpunkt;
		}
	}


