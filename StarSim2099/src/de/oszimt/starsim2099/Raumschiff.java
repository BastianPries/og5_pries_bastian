package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

	// Attribute
	private String typ;
	private String antrieb;
	private int maxLadekapazitaet;
	private int posX;
	private int posY;
	private int winkel;

	// Methoden
	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getAntrieb() {
		return antrieb;
	}

	public void setAntrieb(String antriebsart) {
		this.antrieb = antrieb;
	}

	public int getMaxLadekapazitaet() {
		return maxLadekapazitaet;
	}

	public void setMaxLadekapazitaet(int maxLadekapazitaet) {
		this.maxLadekapazitaet = maxLadekapazitaet;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getWinkel() {
		return winkel;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { { '\0', '\0', '_', '\0', '\0' }, { '\0', '/', 'X', '\\', '\0' },
				{ '\0', '{', 'X', '}', '\0' }, { '\0', '{', 'X', '}', '\0' }, { '/', '_', '_', '_', '\\' }, };
		return raumschiffShape;
	}

}
