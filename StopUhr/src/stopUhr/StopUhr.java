package stopUhr;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 03.11.2016
 * @author Bastian Pries
 */

public class StopUhr {
	private long startpunkt;
	private long endpunkt;

	public StopUhr() {
	}

	public void starten() {
		this.startpunkt = System.currentTimeMillis();
	}

	public void beenden() {
		this.endpunkt = System.currentTimeMillis();
	}

	public long getDauer() {
		return this.endpunkt - this.startpunkt;
	}
}
