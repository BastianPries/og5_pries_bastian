package stopUhr;

/**
*
* Beschreibung
*
* @version 1.0 vom 03.11.2016
* @author Bastian Pries
*/

import java.util.Scanner;

public class Testmath {
	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte n eingeben");

		int n = tastatur.nextInt();
		boolean[] prim = new boolean[(int) (10 * Math.pow(10, n - 1))];

		StopUhr Uhr = new StopUhr();
		Uhr.starten();

		for (int i = (int) (1 * Math.pow(10, n - 1)); i < (10 * Math.pow(10, n - 1)); i++) {
			int z = i;
			prim[i] = myMath.isPrimzahl(z);
			if (prim[i] == true) {
				System.out.println(i);
			}
		}
		Uhr.beenden();
		System.out.println("es hatt " + Uhr.getDauer() + " ms gedauert");

		tastatur.close();
	}

}
