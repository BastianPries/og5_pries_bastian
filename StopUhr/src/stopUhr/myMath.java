package stopUhr;

/**
 *
 * Beschreibung
 *
 * @version 2.0 vom 03.11.2016
 * @author Bastian Pries
 */

public class myMath {

	static boolean isPrimzahl(long zahl) {
		boolean prim = true;
		for (long i = 2; i <= zahl / 2; i++) {
			if (zahl % i == 0) {
				prim = false;
				break;
			} // end of if

		} // end of if

		return prim;
	}

} // end of class myMath
