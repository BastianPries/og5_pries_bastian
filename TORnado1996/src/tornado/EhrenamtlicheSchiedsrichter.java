package tornado;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 06.10.2016
 * @author Bastian Pries
 */

public class EhrenamtlicheSchiedsrichter extends Person{

	// Anfang Attribute
	private int anzahlGepfiffenerSpiele;
	// Ende Attribute

	public EhrenamtlicheSchiedsrichter() {
		this.anzahlGepfiffenerSpiele = 0;
	}

	// Anfang Methoden
	public int getAnzahlGepfiffenerSpiele() {
		return anzahlGepfiffenerSpiele;
	}

	public void setAnzahlGepfiffenerSpiele(int anzahlGepfiffenerSpiele) {
		this.anzahlGepfiffenerSpiele = anzahlGepfiffenerSpiele;
	}

	// Ende Methoden
} // end of EhrenamtlicheSchiedsrichter
