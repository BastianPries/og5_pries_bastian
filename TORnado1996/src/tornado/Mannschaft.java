package tornado;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 06.10.2016
 * @author Bastian Pries
 */

public class Mannschaft {

	// Anfang Attribute
	private String name;
	private String spielklasse;
	private int anzahlSpieler;
	// Ende Attribute

	public Mannschaft() {
		this.name = "";
		this.spielklasse = "";
		this.anzahlSpieler = 0;
	}

	// Anfang Methoden
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpielklasse() {
		return spielklasse;
	}

	public void setSpielklasse(String spielklasse) {
		this.spielklasse = spielklasse;
	}

	public int getAnzahlSpieler() {
		return anzahlSpieler;
	}

	public void setAnzahlSpieler(int anzahlSpieler) {
		this.anzahlSpieler = anzahlSpieler;
	}

	// Ende Methoden
} // end of Mannschaft
