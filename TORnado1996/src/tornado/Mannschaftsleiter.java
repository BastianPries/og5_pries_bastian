package tornado;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 06.10.2016
 * @author Bastian Pries
 */

public class Mannschaftsleiter extends Spieler {

	// Anfang Attribute
	private double rabattProzent;
	// Ende Attribute

	public Mannschaftsleiter() {
		this.rabattProzent = 0;
	}

	// Anfang Methoden
	public double getRabattProzent() {
		return rabattProzent;
	}

	public void setRabattProzent(double rabattProzent) {
		this.rabattProzent = rabattProzent;
	}

	// Ende Methoden
} // end of Mannschaftsleiter
