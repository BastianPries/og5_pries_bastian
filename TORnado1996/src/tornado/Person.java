package tornado;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 06.10.2016
 * @author Bastian Pries
 */

public abstract class Person {

	// Anfang Attribute
	private String name;
	private String telefonnummer;
	private boolean jahresbetragBezahlt;
	// Ende Attribute

	public Person() {
		this.name = "";
		this.telefonnummer = "";
		this.jahresbetragBezahlt = false;
	}

	// Anfang Methoden
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean getJahresbetragBezahlt() {
		return jahresbetragBezahlt;
	}

	public void setJahresbetragBezahlt(boolean jahresbetragBezahlt) {
		this.jahresbetragBezahlt = jahresbetragBezahlt;
	}

	// Ende Methoden
} // end of TORnado1996
