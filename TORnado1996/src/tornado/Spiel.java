package tornado;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 06.10.2016
 * @author Bastian Pries
 */

public class Spiel {

	// Anfang Attribute
	private boolean heimmannschaft;
	private String zeitpunkt;
	private int heimtore;
	private int gasttore;
	private Spieler spielermitgelberKarte;
	private Spieler spielermitroterKarte;
	// Ende Attribute

	public Spiel() {
		this.heimmannschaft = false;
		this.zeitpunkt = "";
		this.heimtore = 0;
		this.gasttore = 0;
		this.spielermitgelberKarte = null;
		this.spielermitroterKarte = null;
	}

	// Anfang Methoden
	public boolean getHeimmannschaft() {
		return heimmannschaft;
	}

	public void setHeimmannschaft(boolean heimmannschaft) {
		this.heimmannschaft = heimmannschaft;
	}

	public String getZeitpunkt() {
		return zeitpunkt;
	}

	public void setZeitpunkt(String zeitpunkt) {
		this.zeitpunkt = zeitpunkt;
	}

	public int getHeimtore() {
		return heimtore;
	}

	public void setHeimtore(int heimtore) {
		this.heimtore = heimtore;
	}

	public int getGasttore() {
		return gasttore;
	}

	public void setGasttore(int gasttore) {
		this.gasttore = gasttore;
	}

	public Spieler getSpielermitgelberKarte() {
		return spielermitgelberKarte;
	}

	public void setSpielermitgelberKarte(Spieler spielermitgelberKarte) {
		this.spielermitgelberKarte = spielermitgelberKarte;
	}

	public Spieler getSpielermitroterKarte() {
		return spielermitroterKarte;
	}

	public void setSpielermitroterKarte(Spieler spielermitroterKarte) {
		this.spielermitroterKarte = spielermitroterKarte;
	}

	// Ende Methoden
} // end of Spiel
