package tornado;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 06.10.2016
 * @author Bastian Pries
 */

public class Spieler extends Person {

	// Anfang Attribute
	private int trikonummer;
	private String spielposition;
	private String mannschaftsname;
	private Mannschaft mannschaft;
	// Ende Attribute

	public Spieler() {
		this.trikonummer = 0;
		this.spielposition = "";
		this.mannschaftsname = "";
		this.mannschaft = null;
	}

	// Anfang Methoden
	public int getTrikonummer() {
		return trikonummer;
	}

	public void setTrikonummer(int trikonummer) {
		this.trikonummer = trikonummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public String getMannschaftsname() {
		return mannschaftsname;
	}

	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;

	}

	public Mannschaft getMannschaft() {

		return this.mannschaft;
	}

	public void manschaftbeitreten(Mannschaft mannschaft1) {
		if (this.mannschaft == null) {
			this.mannschaft = mannschaft1;
			System.out.println("Spieler " + this.getName() + " ist der Mannschaft " + mannschaft1.getName() + " beigetreten");
		} 
		
		else {
			System.out.println("Spieler ist bereits in Mannschaft");
	}
	}
	
	public void mannschaftVerlassen(){
		String Mannschaft = this.mannschaft.getName();
		this.mannschaft = null;
		System.out.println("spieler "+ this.getName() + " hat die Mannschaft " + Mannschaft +  " verlassen");
	}
}

// Ende Methoden
// end of Spieler
