package tornado;

public class Spielerwechsel {

	public static void main(String[] args) {
		// Spieler erstellen
		Spieler spieler01 = new Spieler();
		spieler01.setName("Paul");
		Spieler spieler02 = new Spieler();
		spieler02.setName("Nicklas");
		Spieler spieler03 = new Spieler();
		spieler03.setName("Tom");
		Spieler spieler04 = new Spieler();
		spieler04.setName("Eric");

		// Mannschaft erstellen
		Mannschaft mannschaft01 = new Mannschaft();
		mannschaft01.setName("fcTest");
		Mannschaft mannschaft02 = new Mannschaft();
		mannschaft02.setName("fcTest2");
		
		
		// Trainer erstellen
		Trainer Trainer01 = new Trainer();
		Trainer01.setName("kenny");
		Trainer Trainer02 = new Trainer();
		Trainer02.setName("Otto");
		
		//Spieler Mannschaften zuweisen
		System.out.println("Spieler treten den Manschaften bei");
		System.out.println("");
		spieler01.manschaftbeitreten(mannschaft01);
		spieler02.manschaftbeitreten(mannschaft01);
		spieler03.manschaftbeitreten(mannschaft02);
		spieler04.manschaftbeitreten(mannschaft02);
		
		// Trainer Manschaften zuweisen
		System.out.println("Trainer treten Mannschaften Bei");
		System.out.println("");
		Trainer01.mannschaftBeitreten(mannschaft01);
		Trainer02.mannschaftBeitreten(mannschaft02);
		
		// Spieler wechseln Mannschaften
		System.out.println("spieler wechseln nun die Mannschaften");
		System.out.println("");
		spieler01.mannschaftVerlassen();
		spieler01.manschaftbeitreten(mannschaft02);
		spieler02.mannschaftVerlassen();
		spieler02.manschaftbeitreten(mannschaft02);
		spieler03.mannschaftVerlassen();
		spieler03.manschaftbeitreten(mannschaft01);
		spieler04.mannschaftVerlassen();
		spieler04.manschaftbeitreten(mannschaft01);
		
		// Trainer wechseln Mannschaften
		System.out.println("Trainer wechseln nun die Mannschaften");
		System.out.println("");
		Trainer01.mannschaftVerlassen();
		Trainer01.mannschaftBeitreten(mannschaft02);
		Trainer02.mannschaftVerlassen();
		Trainer02.mannschaftBeitreten(mannschaft01);
		
	}

}
