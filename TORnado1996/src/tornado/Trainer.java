package tornado;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 06.10.2016
 * @author
 */

public class Trainer extends Person {

	// Anfang Attribute
	private char lizenzklasse;
	private double monatlicheAufwandentschaedigung;
	private Mannschaft mannschaft;
	// Ende Attribute

	public Trainer() {
		this.lizenzklasse = '\0';
		this.monatlicheAufwandentschaedigung = 0;
		this.mannschaft = null;
	}

	// Anfang Methoden
	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public double getMonatlicheAufwandentschaedigung() {
		return monatlicheAufwandentschaedigung;
	}

	public void setMonatlicheAufwandentschaedigung(double monatlicheAufwandentschaedigung) {
		this.monatlicheAufwandentschaedigung = monatlicheAufwandentschaedigung;
	}

	public void mannschaftBeitreten(Mannschaft mannschaft1) {

		this.mannschaft = mannschaft1;
		System.out.println("Trainer "+ this.getName() + " trainiert nun die Mannschaft " + mannschaft1.getName());

	}

	public void mannschaftVerlassen() {
		this.mannschaft = null;
		System.out.println("trainer "+ this.getName() + " Trainiert nun keine Mannschaft mehr");
	}
	// Ende Methoden
} // end of Trainer
