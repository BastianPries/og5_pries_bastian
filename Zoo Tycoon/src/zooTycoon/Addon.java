package zooTycoon;

public class Addon {
	
	// Start Attributes
	  private String bezeichnung;
	  private int id;
	  private double preis;
	  private int bestand;
	  private int maxbestand;
	  private double gesamtwert;
	  // End Attributes
	  
	  public Addon() {
	    this.bezeichnung = "";
	    this.id = 0;
	    this.preis = 0;
	    this.bestand = 0;
	    this.maxbestand = 0;
	    this.gesamtwert = 0;
	  }

	  public Addon(String bezeichnung, int id, double preis, int bestand, int maxbestand) {
	    this.bezeichnung = bezeichnung;
	    this.id = id;
	    this.preis = preis;
	    this.bestand = bestand;
	    this.maxbestand = maxbestand;
	  
	  }

	
	// Start Methods
	  public String getBezeichnung() {
	    return bezeichnung;
	  }

	  public void setBezeichnung(String bezeichnung) {
	    this.bezeichnung = bezeichnung;
	  }

	  public int getId() {
	    return id;
	  }

	  public void setId(int id) {
	    this.id = id;
	  }

	  public double getPreis() {
	    return preis;
	  }

	  public void setPreis(double preis) {
	    this.preis = preis;
	  }

	  public int getBestand() {
	    return bestand;
	  }

	  public void setBestand(int bestand) {
	    this.bestand = bestand;
	  }

	  public int getMaxbestand() {
	    return maxbestand;
	  }

	  public void setMaxbestand(int maxbestand) {
	    this.maxbestand = maxbestand;
	  }
	  
	  public void verbrauchen(int anzahl){
		this.bestand -= anzahl;  
	  }
	  
	  public void kaufen(int anzahl){
		  this.bestand += anzahl;
	  }
	  public int maxbestandErmitteln(){
		  return this.maxbestand;
	  }
	  
	  public double gesammtwertErmitteln(double preis , int bestand){
		  
		 return this.gesamtwert = preis * bestand; 
	  }
	  
	 
	  
	  // End Methods
}
