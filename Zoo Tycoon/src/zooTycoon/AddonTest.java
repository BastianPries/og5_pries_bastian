package zooTycoon;

import java.util.Scanner;

public class AddonTest {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);

		// Variablen erstellen
		System.out.println("bezeichnung eingeben");
		String bezeichnung = scan.next();
		System.out.println("Addon id eingeben");
		int id = scan.nextInt();
		System.out.println("Preis eingeben");
		double preis = scan.nextDouble();
		System.out.println("bestand eingeben");
		int bestand = scan.nextInt();
		System.out.println("maximalen bestand angeben");
		int maxbestand = scan.nextInt();

		// Object erstellen
		Addon Addon01 = new Addon(bezeichnung, id, preis, bestand, maxbestand);
		
		

        System.out.println("wie viel " + Addon01.getBezeichnung() + " Kaufen");
		Addon01.kaufen(scan.nextInt());
		System.out.println("der neue Bestand ist:" + Addon01.getBestand());
		System.out.println("wie viel Verbrauchen?");
		Addon01.verbrauchen(scan.nextInt());
		System.out.println("der neue Bestand ist:" + Addon01.getBestand());
		System.out.println("der neue Gesamtwert betr�gt:" + Addon01.gesammtwertErmitteln(Addon01.getPreis(),Addon01.getBestand()) + " $");
		
	}
}
