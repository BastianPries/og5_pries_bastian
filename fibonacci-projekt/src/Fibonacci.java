/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor:
   Version: 1.0
   Datum:
*/
public class Fibonacci {
	// Konstruktor
	Fibonacci() {
	}

	

	/**
	 * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
	 * 
	 * @param n
	 * @return die n-te Fibonacci-Zahl
	 */
	long fiboRekursiv(int n) {
		// vorl�ufig:
     if(n<=0)
		return 0;
     else if (n==1) {
		return 1;
	}
     else 
     return fiboRekursiv (n-2) + fiboRekursiv (n-1) ;
	}// fiboRekursiv

	/**
	 * Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
	 * 
	 * @param n
	 * @return die n-te Fibonacci-Zahl
	 */
	long fiboIterativ(int n) {
		int j = 1;
		int e = 0;
		int helpj;
		int helpe;
		for (int i = 1; i <= n; i++) {
			helpj = j;
			helpe = e;
			e = j + e;
			j = helpe;
		}
		return e;
	}// fiboIterativ

}// Fibonnaci
